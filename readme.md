Nama : Maulana Pandudinata
NIM : 222011379
Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 7

1. Membuka laman simplesaml di http://localhost/simplesaml
![logo](/dokumentasi/simplesaml-1.png)


2. Dilakukan input ke database
![logo](/dokumentasi/simplesaml-2.png)

3. Setelah di lakukan error handling, buka halaman authentication -> Test configured authentication sources -> example-sql (Terbuka laman login seperti berikut)
![logo](/dokumentasi/simplesaml-3.png)

4. Jika salah memasukkan username/password maka akan muncul notifikasi berikut
![logo](/dokumentasi/simplesaml-4.png)

5. Jika berhasil login maka akan muncul seperti laman berikut
![logo](/dokumentasi/simplesaml-5.png)

6. Setelah melakukan login, kemudian membuka http://localhost/myweb1 maka akan muncul seperti laman berikut
![logo](/dokumentasi/simplesaml-6.png)

7. Jika membuka http://localhost/myweb2 maka akan muncul seperti laman berikut
![logo](/dokumentasi/simplesaml-7.png)

